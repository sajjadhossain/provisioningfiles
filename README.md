###Directory Overview
<small>**bold** indicates directory</small>

**Media**

&#9494; **Vid**

<span style="padding-left:15px;"></span> &#9494; behat.mov

<span style="padding-left:15px;"></span> &#9494; Evolution from Quality Assurance to Test Engineering.mp4

<span style="padding-left:15px;"></span> &#9494; Scope Changes - Project Management.mp4

<span style="padding-left:15px;"></span> &#9494; The Expert (Short Comedy Sketch).mp4

<span style="padding-left:15px;"></span> &#9494; Dilbert - Why Projects Fail.mp4

<span style="padding-left:15px;"></span> &#9494; Test Engineering at Google - It's Not QA.mp4

&#9494; **Img**

<span style="padding-left:15px;"></span> &#9494; howToGoogle.gif

**Apps**

For security reasons I removed my personal signed applications but you can use your own to download the below from iTunes or App Store. 
   1. **Download the Applications on the iPad.** 
   * **Backup all applications and settings to the Mac before Factory Reset**. 
   * Then see [how to find my iOS Applications](http://osxdaily.com/2011/12/15/where-ios-apps-stored-locally-in-mac-os-x-and-windows/).

&#9494; iBooks

&#9494; Chrome

&#9494; Dropbox

&#9494; iBooks

&#9494; Google Drive

&#9494; Google Maps

&#9494; Hangouts

&#9494; Opera Mini 


**Profiles**

&#9494; App Restrictions.mobileconfig

&#9494; Test Profile.mobileconfig

&#9494; Web Content Filter.mobileconfig